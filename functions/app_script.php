<?php
/*
Name : Appelles TCHINDA
Date created : 30/08/2019
Code : PHP
*/

function getVisIpAddr() 
{ 
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) { 
        return $_SERVER['HTTP_CLIENT_IP']; 
    } 
    else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { 
        return $_SERVER['HTTP_X_FORWARDED_FOR']; 
    } 
    else { 
        return $_SERVER['REMOTE_ADDR']; 
    } 
} 
 
function getVisInformation() 
{ 
	// Store the IP address 
	$vis_ip = getVisIPAddr(); 
	   
	// Use JSON encoded string and converts 
	// it into a PHP variable 
	$ipdat = @json_decode(file_get_contents( 
		"http://www.geoplugin.net/json.gp?ip=" . $vis_ip)); 
	   
	// echo 'Ip Adress: ' . $vis_ip. "<br/>"; 
	// echo 'Country Name: ' . $ipdat->geoplugin_countryName . "<br/>"; 
	// echo 'Country Code: ' . $ipdat->geoplugin_countryCode . "<br/>"; 
	// echo 'City Name: ' . $ipdat->geoplugin_city . "<br/>"; 
	// echo 'Continent Name: ' . $ipdat->geoplugin_continentName . "<br/>"; 
	// echo 'Latitude: ' . $ipdat->geoplugin_latitude . "<br/>"; 
	// echo 'Longitude: ' . $ipdat->geoplugin_longitude . "<br/>"; 
	// echo 'Currency Symbol: ' . $ipdat->geoplugin_currencySymbol . "<br/>"; 
	// echo 'Currency Code: ' . $ipdat->geoplugin_currencyCode . "<br/>"; 
	// echo 'Timezone: ' . $ipdat->geoplugin_timezone; 
	
	// return "";
	return $ipdat->geoplugin_countryCode;
}
?>