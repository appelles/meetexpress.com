<?php
 include "functions/app_script.php" ;
 $countryCodeVisitor = getVisInformation(); 
?>

<!DOCTYPE html>
<html lang="zxx">
  <head>
  <!-- Basic Page Needs
  ================================================== -->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <!-- Mobile Specific Metas
  ================================================== -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- For Search Engine Meta Data  -->
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <meta name="author" content="yoursite.com" />
	
  <title>Meetexpress : Bienvenue</title>

  <!-- Favicon -->
  <link rel="shortcut icon" type="image/icon" href="images/favicon-16x16.png"/>
   
  <link rel="stylesheet" href="intlTelInput/css/intlTelInput.css"> 
  <!-- Main structure css file -->
  <link  rel="stylesheet" href="css/style.css">
  
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if IE]>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  
	<script src="https://sdk.accountkit.com/en_US/sdk.js"></script> 
  
  </head>
  
  <body class="">
    <!-- Start Preloader --
    <div id="preload-block">
      <div class="square-block"></div>
    </div>
    <!-- Preloader End -->
    
    <div class="container-fluid">
      <div class="row">
        <div class="authfy-container col-xs-12 col-sm-10 col-md-8 col-lg-6 col-sm-offset-1 col-md-offset-2 col-lg-offset-3">
          <div class="col-sm-5 authfy-panel-left">
            <div class="brand-col">
              <div class="headline">
                <!-- brand-logo start -->
                <div class="brand-logo">
					<a href="idex.php">
						<img src="images/brand-logo-white.png" width="150" alt="brand-logo">
					</a>
                </div><!-- ./brand-logo -->
                <p>Site de rencontre instantanée. <?php //echo $countryCodeVisitor; ?></p>
                <!-- social login buttons start -->
                <!--<div class="row social-buttons">
                  <div class="col-xs-4 col-sm-4 col-md-12">
                    <a href="#" class="btn btn-block btn-facebook">
                      <i class="fa fa-facebook"></i> <span class="hidden-xs hidden-sm">Signin with facebook</span>
                    </a>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-12">
                    <a href="#" class="btn btn-block btn-twitter">
                      <i class="fa fa-twitter"></i> <span class="hidden-xs hidden-sm">Signin with twitter</span>
                    </a>
                  </div>
                  <div class="col-xs-4 col-sm-4 col-md-12">
                    <a href="#" class="btn btn-block btn-google">
                      <i class="fa fa-google-plus"></i> <span class="hidden-xs hidden-sm">Signin with google</span>
                    </a>
                  </div>
                </div>--><!-- ./social-buttons -->
				
              </div>
            </div>
          </div>
          <div class="col-sm-7 authfy-panel-right">
            <!-- authfy-login start -->
            <div class="authfy-login">
              <!-- panel-login start -->
              <div class="authfy-panel panel-login text-center active">
                <div class="authfy-heading">
                  <h3 class="auth-title"id="titleLogin">Connectez-vous !</h3>
                  <h3 class="auth-title" id="titleLogin2" style="display:none">Votre compte a été créer avec succès !</h3>
                  <p id="linkCreateAccount">Pas de compte ? <a class="lnk-toggler" data-panel=".panel-signup" href="#">Creer un compte !</a></p>
                  <p id="linkCreateAccount2" style="display:none">Veuillez saisir votre numéro de téléphone utilisé lors de la création du compte.</p>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-sm-12"> 
					<form class="loginForm"  method="post" accept-charset="utf-8">
                      
					  <br/>
					  <br/>
					  <div class="form-group"> 
					  <input id="validLogin"  type="hidden">
						<input id="phoneUserLogin" class="form-control phoneUserLogin"  value="699312490"  name="username" type="tel" style="">
                      </div> 
					  <br/>
                      <!-- start remember-row --> 
                      <div class="form-group">
						<span id="loginProgress"  style="display:none">Veuillez patienter ...
							<img src="images/waiting.gif" width="150"> 
						</span>
                        <button class="btn btn-lg btn-primary btn-block" id="btnLogin" type="button">Me connecter</button>
						
                        <button class="btn btn-lg btn-primary btn-block" id="btnLogin2" style="background:green;display:none" type="button">Connexion réussie</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div> <!-- ./panel-login -->
			   
              <!-- panel-signup start -->
              <div class="authfy-panel panel-signup text-center">
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    <div class="authfy-heading">
                      <h3 class="auth-title">Creer un compte !</h3>
                    </div>
                    <form name="signupForm" class="signupForm" action="#" method="POST">
                      <div class="form-group">
                        <input id="fnameUserRegister"  type="text" class="form-control" value="Appelles TCHINDA" name="fullname" placeholder="Nom & prénom">
					  <input id="validRegister"  type="hidden">
                      </div>
                      <div class="form-group"> 
						<input id="phoneUserRegister" class="form-control phoneUserRegister"  value="699312490"  name="username" type="tel" style="">
                      </div> 
					  <br/>
					  <br/>
                      <div class="form-group">
                        <p class="term-policy text-muted small">En cliquant sur <strong>Créer mon compte</strong>, j'accepte les <a href="#">conditions d'utilisations</a>.</p>
                      </div>
                      <div class="form-group" style="margin-top:-10px;">
						<span id="signProgress"  style="display:none">Veuillez patienter ...
							<img src="images/waiting.gif" width="150"> 
						</span>
                        <button class="btn btn-lg btn-primary btn-block" id="btnSign" type="button">Créer mon compte</button>
                      </div>
                    </form>
                    <a class="lnk-toggler alreadyAccount" data-panel=".panel-login" href="#">J'ai déja un compte</a>
                  </div>
                </div>
              </div> <!-- ./panel-signup -->
              <!-- panel-forget start -->
              <div class="authfy-panel panel-forgot">
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    <div class="authfy-heading">
                      <h3 class="auth-title">Réinitialiser le mot de passe</h3>
                      <p>Renseigner votre addresse e-mail et nous vous enverrons un lien pour réinitialiser le mot de passe.</p>
                    </div>
                    <form name="forgetForm" class="forgetForm" action="#" method="POST">
                      <div class="form-group">
                        <input type="email" class="form-control" name="username" placeholder="Votre addresse e-mail">
                      </div>
                      <div class="form-group">
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Envoyer le lien</button>
                      </div>
                      <div class="form-group">
                        <a class="lnk-toggler" data-panel=".panel-login" href="#">J'ai déja un compte</a>
                      </div>
                      <div class="form-group">
                        <a class="lnk-toggler" data-panel=".panel-signup" href="#">Créer mon compte</a>
                      </div>
                    </form>
                  </div>
                </div>
              </div> <!-- ./panel-forgot -->
            </div> <!-- ./authfy-login -->
          </div>
        </div>
      </div> <!-- ./row -->
    </div> <!-- ./container -->
    
    <!-- Javascript Files -->

    <!-- initialize jQuery Library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 
	<!--<script src="js/jquery.min"></script>-->
  
    <!-- for Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
  
    <!-- Custom js-->
    <script src="js/custom.js"></script>
	
    <!-- dial phone flags js-->
	<script src="intlTelInput/js/intlTelInput.js"></script> 
	
    <!-- Appelles script js-->
	<script src="functions/app_script.js"></script> 
	
    <!-- Script use in the projet js-->
	<script src="js/projet_script.js"></script>

<script>


</script>
	
	<script>
	var input = document.querySelector(".phoneUserLogin");  
	var iti = window.intlTelInput(input, {
		allowDropdown: false,
		// autoHideDialCode: true,
		// autoPlaceholder: "off",
		// dropdownContainer: document.body,
		// excludeCountries: ["us"],
		formatOnDisplay: true, 
		initialCountry: "<?php echo $countryCodeVisitor; ?>",
		// hiddenInput: "full_number",
		// localizedCountries: { 'cm': 'Cameroon' },
		nationalMode: true,
		// onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
		// placeholderNumberType: "MOBILE",
		// preferredCountries: ['cn', 'jp'],
		separateDialCode: true,
		utilsScript: "intlTelInput/js/utils.js",
	}); 
	
	// on blur: validate
	input.addEventListener('blur', function() { 
	  if (input.value.trim()) {
		if (iti.isValidNumber()) {
			$(".phoneUserLogin").css("border-color", "green"); 
			$('#validLogin').val("1");
			// alert("1");
		} else {
			$(".phoneUserLogin").css("border-color", "red"); 
			$('#validLogin').val("0"); 
			// alert("0");
		}
	  }
	});
	
	var inputSign = document.querySelector(".phoneUserRegister");
		window.intlTelInput(inputSign, {
		allowDropdown: true, 
		formatOnDisplay: true,
		initialCountry: "<?php echo $countryCodeVisitor; ?>", 
		separateDialCode: true,
		nationalMode: true,
		utilsScript: "intlTelInput/js/utils.js",
	}); 
	
		// on blur: validate
	inputSign.addEventListener('blur', function() { 
	  if (inputSign.value.trim()) {
		if (iti.isValidNumber()) {
			$(".phoneUserRegister").css("border-color", "green"); 
			$('#validRegister').val("1");
		} else {
			$(".phoneUserRegister").css("border-color", "red"); 
			$('#validRegister').val("0"); 
			// alert("0");
		}
	  }
	});
	
	</script>  
  </body>	
</html>
