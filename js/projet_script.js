/*
Name : Appelles TCHINDA
Date created : 30/08/2019
*/

  
var Script = function () 
{
	
	phoneElementLogin = $("#phoneUserLogin");
	passwElementLogin = $("#passwordUserLogin");
	
	fnameElementSign = $("#fnameUserRegister");
	phoneElementSign = $("#phoneUserRegister");
	passwElementSign = $("#passwordUserRegister");
	
	
	fnameElementSign.focusin(function() { 
		// $(this).removeClass("error_input").addClass("valid_input");
			$(this).css("border-color", "#bcc2ce"); 
	}); 
	phoneElementSign.focusin(function() { 
		// $(this).removeClass("error_input").addClass("valid_input");
			$(this).css("border-color", "#bcc2ce"); 
	}); 
	passwElementSign.focusin(function() { 
		// $(this).removeClass("error_input").addClass("valid_input");
			$(this).css("border-color", "#bcc2ce"); 
	}); 
	
	
	phoneElementLogin.focusin(function() { 
		$(this).css("border-color", "#bcc2ce");
	}); 	
	passwElementLogin.focusin(function() {
		$(this).css("border-color", "#bcc2ce");
	}); 	
	
	
	
	// Lorsque je tape la touche "Entrer" du clavier
	phoneElementLogin.keypress(function(event) 
	{
		var keycode = (event.keyCode ? event.keyCode : event.which );
		if(keycode == 13)	
			loginVisitor();
		event.stopPropagation();
	});  
	  
	passwElementLogin.keypress(function(event) 
	{
		var keycode = (event.keyCode ? event.keyCode : event.which ); 
		if(keycode == '13')	
			loginVisitor();
		event.stopPropagation();
	});
	
	 
	
	// Lorsque je clique pour me connecter
	$("#btnLogin").click(function(){
		//alert($('#phoneUserLogin').val());
		loginVisitor();
	});
	
	// Lorsque je clique pour creer un compte
	$("#btnSign").click(function(){
		registerVisitor();
	});
	
	  // initialize Account Kit with CSRF protection
	AccountKit_OnInteractive = function(){
    AccountKit.init(
      {
        appId:"431065734482138", 
        state:"322323nn2j32jn32jn3", 
        version:"v1.1",
        fbAppEventsEnabled:true,
        redirect:"http://35.180.214.81/AccountKit/"
      }
    );
  };
  

}();

// Connexion de l'utilisateur
function loginVisitor()
{
	
	phoneElement = $("#phoneUserLogin"); 
	validLogin = $('#validLogin').val(); 
	
	if(validLogin == '0')
	{
		phoneElement.css("border-color", "red"); 
	}
	else
	{ 
 		// $("#btnLogin").hide(); 
		// $("#loginProgress").show();  
		smsLogin();
 		 
	} 
 
}
 
 
// Rediriger l'utilisateur apres sa connection
function redirectVisitorAfterLogin()
{
	// window.location.replace("index.php");
	
	$(".panel-login ").removeClass("active"); 
	$(".panel-valid-code").addClass("active"); 
}
  
// Rediriger l'utilisateur apres son inscription
function redirectVisitorAfterRegister()
{
	$(".alreadyAccount").show(); 
	$("#btnSign").show(); 
	$("#signProgress").hide(); 
	 
	smsRegister();
}
 
// Inscription de l'utilisateur
function registerVisitor()
{
	fnameElement = $("#fnameUserRegister");
	phoneElement = $("#phoneUserRegister"); 
	$(".alreadyAccount").show();
	$("#btnSign").show(); 
	
	fnameEnter = fnameElement.val();
	phoneEnter = phoneElement.val(); 
	
	validRegister = $('#validRegister').val(); 
	
	if(validRegister == '0')
	{
		phoneElement.css("border-color", "red"); 
	}
	else
	{  
		// Controle si valeur a ete saisis 
		fnameValidate = isEmpty(fnameEnter); 

		phoneElement.css("border-color", "green");  
		fnameElement.css("border-color", "green"); 
		// Si tout est ok, Connexion
		if(fnameValidate)
		{
			$(".alreadyAccount").hide(); 
			// $("#btnSign").hide(); 
			// $("#signProgress").show(); 
			redirectVisitorAfterRegister();
			// setTimeout(redirectVisitorAfterRegister, 5000);
		}
		// Sinon on affiche les messages d'erreur
		else
		{ 
			// Cas pour le mot de passe
			if(!fnameValidate){ 
				fnameElement.css("border-color", "red"); 
				// fnameValidate.removeClass("valid_input").addClass("error_input"); 
			} 
		}
	}
 
}

  // login callback
  function loginCallback(response) {
    if (response.status === "PARTIALLY_AUTHENTICATED") {
		var code = response.code;
		var csrf = response.state;
		// alert(code+"redirection..."+csrf);
		$("#btnLogin").hide(); 
		// $("#btnLogin").show(); 
		$("#btnLogin2").show();   
		//setTimeout(redirectVisitorAfterLogin, 5000);
		// Send code to server to exchange for access token
    }
    else if (response.status === "NOT_AUTHENTICATED") {
      // handle authentication failure
    }
    else if (response.status === "BAD_PARAMS") {
      // handle bad parameters
    }
  }
 
  // phone form submission handler
  function smsLogin() {
    var countryCode = '+237';
    var phoneNumber = document.getElementById("phoneUserLogin").value;
    AccountKit.login(
      'PHONE', 
      {countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
      loginCallback
    ); 
  }

  

  // login callback
  function registerCallback(response) {
    if (response.status === "PARTIALLY_AUTHENTICATED") {
		var code = response.code;
		var csrf = response.state;

		$(".alreadyAccount").show(); 
		$("#btnSign").show(); 
		$("#signProgress").hide(); 
		
		$(".panel-signup").removeClass("active"); 
		$(".panel-login").addClass("active");  
		
		$("#btnLogin").show(); 
		$("#btnLogin2").hide(); 
		
		$("#titleLogin").hide(); 
		$("#titleLogin2").show(); 
		
		$("#linkCreateAccount").hide(); 
		$("#linkCreateAccount2").show();  
		// Send code to server to exchange for access token
    }
    else if (response.status === "NOT_AUTHENTICATED") {
      // handle authentication failure
    }
    else if (response.status === "BAD_PARAMS") {
      // handle bad parameters
    }
  }
 
 // phone form submission handler
  function smsRegister() {
    var countryCode = '+237';
    var phoneNumber = document.getElementById("phoneUserRegister").value;
    AccountKit.login(
      'PHONE', 
      {countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
      registerCallback
    ); 
  }

  // email form submission handler
  function emailLogin() {
    var emailAddress = document.getElementById("email").value;
    AccountKit.login(
      'EMAIL',
      {emailAddress: emailAddress},
      loginCallback
    );
  } 